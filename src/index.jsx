import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';

import App from './components/App';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory  } from 'history';

import '../assets/stylesheets/application.scss';



// render an instance of the component in the DOM
ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);
