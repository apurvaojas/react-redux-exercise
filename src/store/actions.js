import {
  FETCH_CHARACTERS,
  RECEIVED_CHARACTERS_ERROR,
  RECEIVED_CHARACTERS_SUCCESS,
  FILTER_CHARACTERS,
  SEARCH_CHARACTERS,
  SORT_CHARACTERS,
  SET_CHARACTERS_AFTER_FILTER_SORT,
  SET_FILTER_OPTIONS
} from "./actionTypes";

import {
  filterCharacterByText,
  filterCharactersBy,
  sortCharacterBy,
  getFilterOptions,
  updateFilterOptions
} from "./helpers";

import axios from "axios";

export const fetchCharacters = () => ({
  type: FETCH_CHARACTERS
});

export const receivedCharactersSuccess = payload => ({
  type: RECEIVED_CHARACTERS_SUCCESS,
  payload
});
export const receivedCharactersError = error => ({
  type: RECEIVED_CHARACTERS_ERROR,
  error
});

export const setCharactersAfterFilter = characters => ({
  type: SET_CHARACTERS_AFTER_FILTER_SORT,
  characters
});

export const filterCharacter = (selections = {}) => ({
  type: FILTER_CHARACTERS,
  selections
});

export const searchCharacters = filterText => ({
  type: SEARCH_CHARACTERS,
  filterText
});

export const sortCharacters = key => ({ type: SORT_CHARACTERS, key });

export const setFilterOptions = filterOptions => ({
  type: SET_FILTER_OPTIONS,
  filterOptions
});

export function fetchPosts() {
  return function(dispatch) {
    dispatch(fetchCharacters());
    return axios
      .get(`https://rickandmortyapi.com/api/character/`)
      .then(
        response => response.data.results,
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        dispatch(setFilterOptions(getFilterOptions(json)));
        dispatch(receivedCharactersSuccess(json));
      })
      .catch(error => {
        dispatch(receivedCharactersError(error));
      });
  };
}

export function setFilter(
  { key, selectedFilters, type, filterText },
  characters, filterOptions
) {
  return dispatch => {
    switch (type) {
      case SEARCH_CHARACTERS: {
        dispatch(searchCharacters(filterText));
        return dispatch(
          setCharactersAfterFilter(
            filterCharacterByText(filterText, characters)
          )
        );
      }
      case FILTER_CHARACTERS: {
        dispatch(filterCharacter(selectedFilters));
        dispatch(setFilterOptions(updateFilterOptions(filterOptions, selectedFilters)))
        return dispatch(
          setCharactersAfterFilter(
            filterCharactersBy(selectedFilters, characters)
          )
        );
      }
      case SORT_CHARACTERS: {
        dispatch(sortCharacters(key));
        return dispatch(
          setCharactersAfterFilter(sortCharacterBy(key, characters))
        );
      }
    }
  };
}
