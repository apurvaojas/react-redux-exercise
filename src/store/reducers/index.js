import { combineReducers } from "redux";
import filters from "./charecter-filter";
import characters from "./character";

export default combineReducers({ characters, filters });
