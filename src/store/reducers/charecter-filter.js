import {
  FILTER_CHARACTERS,
  SEARCH_CHARACTERS,
  SORT_CHARACTERS,
  SET_FILTER_OPTIONS
} from "../actionTypes";

const initialState = {
  selectedFilters: {
    species: [],
    gender: [],
    origin: []
  },
  filterOptions: {
    species: [],
    gender: [],
    origin: []
  },
  sort: '',
  filterText: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FILTER_CHARACTERS: {
      return {
        ...state,
        selectedFilters: {...action.selections}
      }
    }
    case SEARCH_CHARACTERS: {
      return {
        ...state,
        filterText: action.filterText
      }
    }
    case SORT_CHARACTERS: {
      return {
        ...state,
        sort: action.key
      }
    }
    case SET_FILTER_OPTIONS: {
      return {
        ...state,
        filterOptions: action.filterOptions
      }
    }
    default: {
      return state;
    }
  }
};

