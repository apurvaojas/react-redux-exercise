import { FETCH_CHARACTERS, RECEIVED_CHARACTERS_SUCCESS, RECEIVED_CHARACTERS_ERROR, SET_CHARACTERS_AFTER_FILTER_SORT } from "../actionTypes";

const initialState = {
  isLoading: true,
  characters: [],
  originalCharacters: [],
  error: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_CHARACTERS: {
      return {
        ...state,
        isLoading: true
      };
    }
    case RECEIVED_CHARACTERS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        originalCharacters: action.payload,
        characters: action.payload
      };
    }
    case RECEIVED_CHARACTERS_ERROR: {
      return {
        ...state,
        isLoading: false,
        error: action.error
      };
    }
    case SET_CHARACTERS_AFTER_FILTER_SORT: {
      return {
        ...state,
        characters: action.characters,
        isLoading: false
      }
    }
    default:
      return state;
  }
}
