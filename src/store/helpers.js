export const sortCharacterBy = (val, characters) => {
  return [
    ...characters.sort((a, b) => {
        return val === 'asc' ? a.id - b.id : b.id - a.id
    })
  ];
};

export const filterCharactersBy = (selectedItems, characters) => {
  let origCharacters = [...characters];
  let filteredCharacter = [];
  for (const key in selectedItems) {
    if (selectedItems[key].length) {
      const uniqArray = [];
      filteredCharacter = (filteredCharacter.length
        ? filteredCharacter
        : characters
      ).filter((char, idx, self) => {
        if (
          selectedItems[key].indexOf(
            key === "origin" ? char[key].name : char[key]
          ) !== -1 &&
          !uniqArray.some(item => item.id === char.id)
        ) {
          uniqArray.push(char);
          return true;
        }
        return false;
      });
    }
  }
  return filteredCharacter.length ? filteredCharacter : characters;
};

export const filterCharacterByText = (text, characters) => {
  return characters.filter(char => String(char.name).toLowerCase().includes(text.toLowerCase()));
};

export const getFilterOptions = payload => {
  return {
    species: payload
      .map(char => char.species)
      .filter((value, index, self) => self.indexOf(value) === index)
      .map(label => ({ label, selected: false })),
    gender: payload
      .map(char => char.gender)
      .filter((value, index, self) => self.indexOf(value) === index)
      .map(label => ({ label, selected: false })),
    origin: payload
      .map(char => char.origin.name)
      .filter((value, index, self) => self.indexOf(value) === index)
      .map(label => ({ label, selected: false }))
  };
};

export const updateFilterOptions = (filterOptions, selectedOptions) => {
  Object.keys(filterOptions).map(key => {
    filterOptions[key] = filterOptions[key].map(op => ({
      ...op,
      selected: selectedOptions[key].indexOf(op.label) !== -1
    }));
  });

  return filterOptions;
};

export const getSelection = (prevSelection, value, isChecked) => {
  const index = prevSelection.indexOf(value);
  if (index !== -1 && !isChecked) {
    prevSelection.splice(index, 1);
    return [...prevSelection];
  }
  prevSelection.push(value);
  return [...prevSelection];
};
