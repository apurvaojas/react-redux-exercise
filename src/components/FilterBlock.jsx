import React from "react";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import {FILTER_CHARACTERS} from '../store/actionTypes';
import {getSelection} from '../store/helpers';


export default function FilterBlock(props) {
    const handleChange = (event) => {
        const {setFilter, selectedFilters, filterBy} = props;
        setFilter({
            type: FILTER_CHARACTERS,
            selectedFilters: {
                ...selectedFilters,
                [filterBy]: getSelection(selectedFilters[filterBy], event.target.value, event.target.checked)
            }

        })
    };
  return (
    <FormGroup>
      {props.filters.map(item => {
        return (
          <FormControlLabel
            control={
              <Checkbox checked={item.selected} onChange={handleChange} value={item.label} />
            }
            label={item.label}
          />
        );
      })}
    </FormGroup>
  );
}
