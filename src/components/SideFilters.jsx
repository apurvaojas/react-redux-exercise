import React from "react";
import FilterBlock from "./FilterBlock";

export default function SideFilters(props) {
  return Object.keys(props.filters).map((filter, idx) => {
    return (
      <div className="border p-2 m-2" key={idx}>
        <h4>{filter}</h4>
        <FilterBlock
          filterBy={filter}
          filters={props.filters[filter]}
          setFilter={props.setFilter}
          selectedFilters={props.selectedFilters}
        />
      </div>
    );
  });
}
