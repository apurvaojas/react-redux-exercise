import React from "react";
import TopFilters from "./TopFilters";
import Item from "./Item";

export default function Content(props) {
  return (
    <div className="row">
      <div className={'col-12'}>
        <TopFilters
          selections={props.selectedFilters}
          setFilter={props.setFilter}
          filterText={props.filterText}
        />
      </div>
      {props.characters.map((char, idx) => (
        <div className="col-sm-6 col-md-4">
          <Item character={char} key={idx} />
        </div>
      ))}
    </div>
  );
}
