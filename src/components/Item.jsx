import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";

const useStyles = makeStyles({
  root: {
    maxWidth: 345
  },
  media: {
    height: 140
  }
});

export default function Item(props) {
  const classes = useStyles();
  const {
    character: { image, name, species, gender, origin, status, id }
  } = props;
  return (
    <Card classes={{root: 'my-2'}}>
      <CardActionArea>
        <CardMedia className={classes.media} image={image} title={name} />
        <CardContent>
          <Typography gutterBottom variant="h3" component="h3">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <List>
            <ListItem>
                <ListItemText primary={`ID`} />
                <ListItemSecondaryAction>
                    {id}
                </ListItemSecondaryAction>
              </ListItem>
              <ListItem>
                <ListItemText primary={`species`} />
                <ListItemSecondaryAction>
                    {species}
                </ListItemSecondaryAction>
              </ListItem>
              <ListItem>
                <ListItemText primary={`status`} />
                <ListItemSecondaryAction>
                    {status}
                </ListItemSecondaryAction>
              </ListItem>
              <ListItem>
                <ListItemText primary={`gender`} />
                <ListItemSecondaryAction>
                    {gender}
                </ListItemSecondaryAction>
              </ListItem>
              <ListItem>
                <ListItemText primary={`origin`} />
                <ListItemSecondaryAction>
                    {origin.name}
                </ListItemSecondaryAction>
              </ListItem>
            </List>
          </Typography>
        </CardContent>
      </CardActionArea>
      {/* <CardActions>
        
      </CardActions> */}
    </Card>
  );
}
