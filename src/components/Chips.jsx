import React from "react";
import Chip from "@material-ui/core/Chip";

export default function Chips(props) {
  
  return (
    <Chip
      label={props.item}
      onDelete={() => props.setFilter(props.item, props.filterBy)}
      className={'m-2'}
    />
  );
}
