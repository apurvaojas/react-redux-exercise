import React from "react";
import SideFilters from "./SideFilters";
import Content from "./Content";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actionCreators from "../store/actions";

class Main extends React.Component {
  constructor() {
    super();
  }

  componentWillMount() {
    const { fetchPosts } = this.props;
    fetchPosts();
  }

  setFilter(options = {}) {
    const {
      characters: { originalCharacters, characters: filteredCharacters },
      setFilter,
      filters: { filterOptions }
    } = this.props;
    setFilter(options, originalCharacters, filterOptions);
  }

  render() {
    console.log("*****************", this.props);
    const {
      characters: { characters },
      filters: { filterOptions, selectedFilters, filterText }
    } = this.props;
    return (
      <div className="row">
        <div className={"col-md-4"}>
          <SideFilters
            filters={filterOptions}
            setFilter={this.setFilter.bind(this)}
            selectedFilters={selectedFilters}
          />
        </div>
        <div className={"col-md-8"}>
          <Content
            characters={characters}
            selectedFilters={selectedFilters}
            setFilter={this.setFilter.bind(this)}
            filterText={filterText}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state
  };
}

function mapDispachToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

const App = connect(mapStateToProps, mapDispachToProps)(Main);

export default App;
