import React from "react";
import Paper from "@material-ui/core/Paper";
import Chips from "./Chips";
import {
  FILTER_CHARACTERS,
  SEARCH_CHARACTERS,
  SORT_CHARACTERS
} from "../store/actionTypes";
import { getSelection } from "../store/helpers";

export default function TopFilters(props) {
  const setFilter = (item, filterBy) => {
    const { setFilter, selections } = props;
    setFilter({
      type: FILTER_CHARACTERS,
      selectedFilters: {
        ...selections,
        [filterBy]: getSelection(selections[filterBy], item, false)
      }
    });
  };

  const onSearch = event => {
    const { setFilter } = props;
    setFilter({
      type: SEARCH_CHARACTERS,
      filterText: event.target.value
    });
  };

  const onSort = event => {
    const { setFilter } = props;
    setFilter({
      type: SORT_CHARACTERS,
      key: event.target.value
    });
  };

  return (
    <Paper>
      <div className="row">
        <div className="col-12">
          <h3>Selected Filters</h3>
          {Object.keys(props.selections).map(key => {
            return props.selections[key].map(item => (
              <Chips item={item} setFilter={setFilter} filterBy={key} />
            ));
          })}
        </div>
        <div className="col-12 col-sm-6">
          <label>Search By Name</label>
          <input type="text" onChange={onSearch} value={props.filterText} />
        </div>
        <div className="col-12 col-sm-6">
            
          <select onChange={onSort}>
            <option value="" disabled selected>Sort by ID</option>    
            <option value={"asc"}>Acending</option>
            <option value={"desc"}>Decending</option>
          </select>
        </div>
      </div>
    </Paper>
  );
}
